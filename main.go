package main

import (
	"log"
	"net/http"

	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
)

func newClient() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})

	return client
}

func handle() {
	r := mux.NewRouter()
	r.HandleFunc("/api/create-device", CreateDevice).Methods("POST")
	r.HandleFunc("/api/get-device/{id}", GetDevice).Methods("GET")
	r.HandleFunc("/api/update-device", UpdateDevice).Methods("PUT")
	r.HandleFunc("/api/delete-device/{id}", DeleteDevice).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":80", r))
}

func main() {
	handle()
}
