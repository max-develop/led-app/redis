package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
)

//helper func to get data from redis
func getFromRedis(w http.ResponseWriter, cl *redis.Client, key string) string {
	val, err := cl.Get(key).Result()
	if err == redis.Nil {
		log.Println("[ERROR] Could not find device: ",err)
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("[ERROR] Could not find device"))
		return ""
	}
	return val
}

//helper func to set data to redis
func setToRedis(w http.ResponseWriter, cl *redis.Client, key string, value string) error {
	err := cl.Set(key, value, 0).Err()
	if err != nil {
		log.Println("[ERROR] Could not set data: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR]: Could not set data"))
		return err
	}
	return nil
}

//CreateDevice handles the creation of a new device
func CreateDevice(w http.ResponseWriter, r *http.Request) {
	dev := NewEmptyDevice()
	//read device info from client
	err := json.NewDecoder(r.Body).Decode(&dev)
	if err != nil {
		log.Println("[ERROR] Could not parse data: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR]: Could not parse data"))
		return
	}

	cl := newClient()
	//convert device struct to JSON
	devJSON, err := json.Marshal(dev)
	if err != nil {
		log.Println("[ERROR] Could not convert struct to JSON: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR]:  Could not convert struct to JSON"))
		return
	}

	//Set device in Redis
	err = setToRedis(w, cl, dev.ID, string(devJSON))
	if err != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("Created Device with ID %s", dev.ID)))
}

//GetDevice handles the reading of devices
func GetDevice(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)["id"]

	dev := NewEmptyDevice()
	cl := newClient()

	//get all devices from redis
	if vars == "*" {
		keys := cl.Keys(vars)
		keySlice := keys.Val()
		devices := make([]Device, len(keySlice))
		for i := 0; i < len(keySlice); i++ {
			val := getFromRedis(w, cl, keySlice[i])

			err := json.Unmarshal([]byte(val), &dev)
			if err != nil {
				log.Println("[ERROR] Could not parse data: ",err)
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte("[ERROR]: Could not parse data"))
				return
			}
			devices = append(devices, *dev)
		}
		msg, err := json.Marshal(devices)
		if err != nil {
			log.Println("[ERROR] Could not parse data: ",err)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("[ERROR]: Could not parse data"))
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(string(msg)))
		return
	}

	val := getFromRedis(w, cl, vars)

	err := json.Unmarshal([]byte(val), &dev)
	if err != nil {
		log.Println("[ERROR] Could not parse data: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR]: Could not parse data"))
		return
	}

	log.Println(dev)

	json.NewEncoder(w).Encode(dev)
}

//UpdateDevice handles the updating of the given device
func UpdateDevice(w http.ResponseWriter, r *http.Request) {
	dev := Device{}
	cl := newClient()

	//read device info from client
	err := json.NewDecoder(r.Body).Decode(&dev)
	if err != nil {
		log.Println("[ERROR] Could not parse data: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR]: Could not parse data"))
		return
	}

	//check if key already exists, abort if not
	val := getFromRedis(w, cl, dev.ID)

	//convert device struct to JSON
	devJSON, err := json.Marshal(dev)
	if err != nil {
		log.Println("[ERROR] Could not set struct: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR]: Could not set struct"))
		return
	}
	//Set device in Redis
	err = setToRedis(w, cl, dev.ID, string(devJSON))
	if err != nil {
		return
	}

	sendToDevice(w, dev)

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("Updated %s to %s", val, dev)))
}

//DeleteDevice handles the deletion of devices
func DeleteDevice(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)["id"]

	cl := newClient()

	if vars == "*" {
		cl.FlushAll()
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("All flushed"))
		return
	}

	//check if key already exists, abort if not
	val := getFromRedis(w, cl, vars)

	//delete key/values
	err := cl.Del(vars).Err()
	if err != nil {
		log.Println("[ERROR] Could not remove device: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR]: Could not remove "))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("Succ removed %s", val)))
}

func sendToDevice(w http.ResponseWriter, dev Device) {
	HTTPclient := &http.Client{}

	//convert device struct to JSON
	devJSON, err := json.Marshal(dev)
	if err != nil {
		log.Println("[ERROR] Could not convert struct to JSON: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR]: Could not convert struct to JSON"))
		return
	}

	serviceName := "led-app-device-rest:80"
	updateURL := fmt.Sprintf("http://%s/device-api/update-device", serviceName)
	//create request to agent
	req, err := http.NewRequest("PUT", updateURL, strings.NewReader(string(devJSON)))
	if err != nil {
		log.Println("[ERROR] Could not create request: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR]: Could not create request"))
		return
	}
	//send request
	_, err = HTTPclient.Do(req)
	if err != nil {
		log.Println("[ERROR] Could not connect to Redis-REST: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR]: Could not connect to Redis-REST"))
		return
	}

}
